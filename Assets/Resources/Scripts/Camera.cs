using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

    private float speed = 5f;
    private float zLockPosition = -3;

    void Update () {
        Vector3 myVector = new Vector3(transform.position.x, transform.position.y, zLockPosition);
        Vector3 moveVector = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0);
        moveVector = moveVector * speed * Time.deltaTime;

        // move the camera
        transform.position = myVector + moveVector;

        // TargetManager is the script that handles selections and clicks to determine what is targeted
        // and to handle drag selection; need to keep its position updated when camera is moved to anchor the
        // selector box
        TargetManager.manager.hitDown = TargetManager.manager.hitDown + moveVector;
        TargetManager.manager.downMousePos = UnityEngine.Camera.main.WorldToScreenPoint(TargetManager.manager.hitDown);
    }
}
